**This is the home page of [this documentation](http://wonderland.eode.studio/docs/fsm-_introduction.html)**

## Introduction

### Version
> Released, stable

## Introduction
FSM is a small plugin that provides a MonoBehaviour based state machine and uses Tasks for enter / exit functions.

## Example
![FSM Hierarchy](http://wonderland.eode.studio/FSM/fsm.png)